<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
//    return redirect('/login');
});

Route::get('/Error_404', function () {
    return view('errors/404_error');
});

Route::get('/Error_500', function () {
    return view('errors/500_error');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('MultiPurposePage',    'Backend\MultiPurposeController');


// Login admin Check===========
Route::get('Login/Admin', 'Auth\AdminLoginController@ShowLoginForm')->name('admin.loginme');

Route::prefix('admin')->group(function () {
    Route::post('loginme', 'Auth\AdminLoginController@Adminlogin')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});


//For permission user create
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;


//Route::group(['middleware' => 'auth:admin'], function(){
//    Route::get('/create_per', function () {
//        $role = Role::create(['name' => 'Super Admin']);
//        $permission = Permission::create(['name' => 'All Access']);
//        auth()->user()->assignRole('Super Admin');
//        auth()->user()->givePermissionTo('All Access');
//    });
//
//});


/*===========Department Entry=============*/
Route::resource('/department', 			'Backend\DepartmentController');


/*===========Semester Entry=============*/
Route::resource('/semester', 			'Backend\SemesterController');


/*===========Student Entry=============*/
Route::resource('/student', 			'Backend\StudentController');

/*===========Teacher Entry=============*/
Route::resource('/teacher', 			'Backend\TeacherController');
Route::get('/getSubjects/{department}', 'Backend\DepartmentController@getSubjects');


/*===========Subject Entry=============*/
Route::resource('/subject', 			'Backend\SubjectController');






// Login User Check===========
//Route::get('portal',                    'PortalController@showLogin');
Route::get('portal',                   'PortalController@userLogin');

//Student personal info update
Route::get('/studentProfile/Update',            'PortalController@studentEditProfile');
Route::post('/studentProfile/Update',           'PortalController@studentUpdateProfile');


//Teachers personal info update
Route::get('/teacherProfile/Update',            'PortalController@teacherEditProfile');
Route::post('/teacherProfile/Update',           'PortalController@teacherUpdateProfile');

Route::get('/passwordChange',                   'PortalController@passwordChangeShow');
Route::post('password-change',                  'PortalController@passwordChange');

Route::resource('/attendance',                            'Backend\AttendenceController');
Route::post('/studentAttendenceList',                     'Backend\AttendenceController@studentList');
Route::get('/getAttendenceSubjects/{department}',         'Backend\AttendenceController@getSubjects');
