
<!-- jQuery 3 -->
<script src="{{asset('backend_assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('backend_assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('backend_assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend_assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend_assets/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('backend_assets/dist/js/demo.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>

<script type="text/javascript">
    $('input,select').keydown(function (e){
        if(e.keyCode == 13){
            var index = $('input,select').index(this) + 1;
            $('input,select').eq(index).focus();
        }
    });


    // Ajax loading===========
    $(document).ajaxStart(function(){
        $('#loader').show();
    }).ajaxStop(function(){
        $('#loader').hide();
    });

    // browser loading=======
    // window.onload = function(){ document.getElementById("loading").style.display = "none" }



    function validationCheck()
    {
        var isvalid = true;
        $('#Form :input[required], select[required]').each(function (){
            var id = this.id;
            if (this.value.trim() === '') {
                $(this).css('border','1px solid red');
                $('#s2id_'+id+'>a').css('border','1px solid red');
                isvalid = false;
            }else{
                $(this).css('border','1px solid #23bdbb');
                $('#s2id_'+id+'>a').css('border','1px solid #23bdbb');
            }
        });

        return isvalid;
    }

    // Live Search DataTable===============
    $("#key").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".Search tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-show').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image").change(function(){
        readURL(this);
    });
</script>

{{--<script type="text/javascript">--}}
{{--    function imageShow() {--}}
{{--        var id = $(this).attr('id');--}}
{{--        alert(id);--}}
{{--        var input = $(this).attr('input');--}}
{{--        if (input.files && input.files[0]) {--}}
{{--            var reader = new FileReader();--}}

{{--            reader.onload = function (e) {--}}
{{--                $('#'+ id + '-show').attr('src', e.target.result);--}}
{{--            }--}}
{{--            reader.readAsDataURL(input.files[0]);--}}
{{--        }--}}

{{--        // $('#'+ id).change(function () {--}}
{{--        //     readURL(this);--}}
{{--        // });--}}
{{--    }--}}

{{--</script>--}}


{{--<!-- Core JS files -->--}}
{{--<script type="text/javascript" src="{{asset('backend_assets/plugins/loaders/pace.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{asset('backend_assets/core/libraries/jquery.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{asset('backend_assets/core/libraries/bootstrap.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{asset('backend_assets/plugins/loaders/blockui.min.js') }}"></script>--}}
{{--<!-- /core JS files -->--}}
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->



<!-- For Fancy Box -->
<script type="text/javascript" src="{{ asset('backend_assets/fancyBox/js/jquery.fancybox.js?v=2.1.5 ') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('backend_assets/fancyBox/css/jquery.fancybox.css?v=2.1.5') }}" media="screen" />
<script type="text/javascript" src="{{ asset('backend_assets/fancyBox/js/fancybox_design.js') }}"></script>
<!-- For Fancy Box End -->



<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Javascript -->
<script>
    $(function() {
        $( ".datepicker" ).datepicker();
        $( ".datepicker" ).datepicker("setDate", "today");
    });
</script>