<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('backend_assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                {{--          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @if(Auth::user()->type == "teacher")
                <li class="treeview @yield('teacher')">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Teacher</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="nav-item @yield('my_profile')"><a href="{{ url('/teacherProfile/Update') }}"><i
                                        class="fa fa-circle-o"></i>My profile</a></li>
                        <li class="nav-item @yield('password_change')"><a href="{{ url('/passwordChange') }}"><i
                                        class="fa fa-circle-o"></i>Password Change</a></li>
                    </ul>
                </li>

                <li class="treeview @yield('attendance')">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Attendance</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="nav-item @yield('add_attendance')"><a href="{{ url('/attendance') }}"><i
                                        class="fa fa-circle-o"></i>Add Attendance</a></li>
{{--                        <li class="nav-item @yield('list_attendance')"><a href="{{ url('/Attendance') }}"><i--}}
{{--                                        class="fa fa-circle-o"></i> List Attendance</a></li>--}}
                    </ul>
                </li>
            @endif

            @if(Auth::user()->type == "student")
                <li class="treeview @yield('student')">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Student</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="nav-item @yield('add_student')"><a href="{{ url('/studentProfile/Update') }}"><i
                                        class="fa fa-circle-o"></i>My Profile</a></li>
                        <li class="nav-item @yield('password_change')"><a href="{{ url('/passwordChange') }}"><i
                                        class="fa fa-circle-o"></i>Password Change</a></li>
                    </ul>
                </li>
            @endif

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>