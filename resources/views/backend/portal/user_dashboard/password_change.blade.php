@extends('backend.portal.user_dashboard.user_dashboard')
@section('teacher', 'active')
@section('password_change', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Teacher</h3>
                </div>
                <!-- form start -->
                <form class="form-horizontal passForm" method="post">
                {{ csrf_field() }}
                    <input type="hidden" name="userID" value="{{ auth()->user()->id }}">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Current Password</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="currentPassword" placeholder="*********"  required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">New Password</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="password"  placeholder="*********" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Confirm Password</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="re-password"  placeholder="*********" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="float: right">
                        <button type="button" style="margin-right: 300px;" onclick="passwordChange()" class="btn btn-success">Update
                            <i class="icon-arrow-right14 position-right"></i></button>
                        <!-- /.box-footer -->
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        /*
 * Password Change
 */
        function passwordChange() {

            $.ajax({
                type: 'post',
                url: '/password-change',
                data: $('.passForm').serialize(),
                dataType: "JSON",
                success: function (data) {
                    if (data.validation) {
                        var msgg = "";
                        $.each(data.validation, function (key, value) {
                            msgg += value + "\n";
                        });
                        validationMessage(msg);
                    }
                    if (data.success) {
                        $('.passForm').find('.pass_text').addClass('show');
                        $('.passForm').find('.pass').removeClass('show');

                        successMessage(data.success);
                        $('.passForm')[0].reset();
                    }
                    if (data.error) {
                        errorMessage(data.error);
                    }
                    if (data.notice) {
                        errorMessage(data.notice);
                    }
                }
            });
        }
    </script>

@endsection