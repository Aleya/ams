@extends('backend.portal.user_dashboard.user_dashboard')
@section('teacher', 'active')
@section('my_profile', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Student</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="#" class="form-horizontal">

                    <fieldset class="content-group">
                        <!-- Write Your Code Here -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="idd" value="{{ $user->id }}">

                        <!-- Form Input Field -->
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Student Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Father's Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="father_name" value="{{$user->father_name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Mother's Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="mother_name" value="{{$user->mother_name}}" required>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" value="{{$user->email}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="contact_number" value="{{$user->contact_number}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="address" value="{{$user->address}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Image</label>

                                <div class="col-sm-6">
                                    <input type="file" name="image" class="form-control image">
                                </div>
                                <div class="col-sm-2">
                                    @if($user->image)

                                        <img src="{{ asset('images/student_images/'.$user->image) }}"
                                             height="50" width="50" class="image-show">
                                    @else
                                        <img src="{{ asset('backend_assets/no_image.png') }}" height="50"
                                             width="50" class="image-show">

                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Signature</label>

                                <div class="col-sm-6">
                                    <input type="file" name="signature" class="form-control image">
                                </div>
                                <div class="col-sm-2">
                                    @if($user->signature)

                                        <img src="{{ asset('images/student_signatures/'.$user->signature) }}"
                                             height="50" width="50" class="image-show">
                                    @else
                                        <img src="{{ asset('backend_assets/no_image.png') }}" height="50"
                                             width="50" class="image-show">

                                    @endif
                                </div>
                            </div>
                        </div>


                    </fieldset>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Update <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                    <!-- /.box-footer -->

                </form>
            </div>
        </div>
    </div>

@endsection

