<!DOCTYPE html>
<html>
@include('backend.portal.partials.head')

<style>
    .edit{
        float: left;
        margin-right: 10px;
    }
    .delete{
        float: left;
    }
</style>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <!-- =============================================== -->
@include('backend.portal.partials.header')
<!-- Left side column. contains the sidebar -->
@include('backend.portal.partials.sidebar')

<!-- =============================================== -->


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content">
            <!-- Main content -->
        @yield('content')
        <!-- /.content -->
        </div>
    </div>

    <!-- /.content-wrapper -->

    @include('backend.portal.partials.footer')



</div>
<!-- ./wrapper -->
@include('backend.portal.partials.js_links')
@include('backend.ajax_request')
@include('backend.msg')
</body>
</html>


