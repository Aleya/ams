@extends('backend.portal.user_dashboard.user_dashboard')
@section('teacher', 'active')
@section('my_profile', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Teacher</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="#" class="form-horizontal">
                {{ csrf_field() }}
                <!-- Request Url -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Teacher Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Student Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="contact_number" value="{{$user->contact_number}}" placeholder="Contact Number" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" value="{{$user->email}}" placeholder="Email" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="address" value="{{$user->address}}" placeholder="Address" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Image</label>

                                <div class="col-sm-6">
                                    <input type="file" name="image" value="{{$user->image}}" id="image" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    @if($user->image)

                                        <img src="{{ asset('images/teacher_images/'.$user->image) }}"
                                             height="50" width="50" class="image-show">
                                    @else
                                        <img src="{{ asset('backend_assets/no_image.png') }}" height="50"
                                             width="50" class="image-show">

                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Update <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>

@endsection