@extends('backend.portal.user_dashboard.user_dashboard')
@section('attendance', 'active')
@section('add_attendance', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Student Attendence</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="/studentAttendenceList" class="form-horizontal">
                {{ csrf_field() }}
                <!-- Request Url -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Department</label>

                                <div class="col-sm-8">
                                    <select name="department_id" onchange="getSubject(this.value)" class="form-control" required>
                                        <option value="">Select</option>
                                        @foreach($getDepartment as $data)
                                            <option value="{{$data->id}}">{{$data->department_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Subject Name</label>

                                <div class="col-sm-8">
                                    <select name="subject_id" id="subjects" class="form-control" required>
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Semester</label>

                                <div class="col-sm-8">
                                    <select name="semester_id" class="form-control" required>
                                        <option>Select</option>
                                        @foreach($getSemester as $data)
                                            <option value="{{$data->id}}">{{$data->semester_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Batch</label>

                                <div class="col-sm-8">
                                    <select name="batch" class="form-control" required>
                                        <option>Select</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Pre-requisite">Pre-requisite</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Section</label>

                                <div class="col-sm-8">
                                    <select name="section" class="form-control" required>
                                        <option>Select</option>
                                        <option value="a">a</option>
                                        <option value="b">b</option>
                                        <option value="c">c</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <label class="col-sm-3 control-label">Date</label>

                                <div class="col-sm-4">
                                    <input type="text" class="datepicker">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="float: right;">
                        <button type="submit" class="btn btn-success">Go Now <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
    <script>
        function getSubject(depID) {
            if (depID == "") {
                return false;
            }
            $.ajax({
                type: "get",
                url: "/getAttendenceSubjects/" + depID,
                dataType: "JSON",
                success: function (subjects) {
                    $('#subjects').html("");

                    $('#subjects').append($('<option>', {
                        value: "",
                        text: "Select any"
                    }));

                    $.each(subjects, function (i, item) {
                        $('#subjects').append($('<option>', {
                            value: item.id,
                            text: item.subject_name
                        }));
                    });
                }
            });

        }
    </script>



@endsection