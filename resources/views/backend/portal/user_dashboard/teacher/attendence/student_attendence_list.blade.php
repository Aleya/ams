@extends('backend.portal.user_dashboard.user_dashboard')
@section('attendance', 'active')
@section('add_attendance', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">List Semester</h3>
                </div>
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 20px">
                            <table id="example2" class="table table-bordered table-hover dataTable" >
                                <thead>
                                <tr role="row">
                                    <th >Student Name</th>
                                    <th >Department</th>
                                    <th >Email</th>
                                    <th >Batch</th>
                                    <th >Section</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                @if ($getStudent)
                                    @foreach ($getStudent as $data)

                                        <tr>
                                            <td style="width: 10%;">{{ $data->name }}</td>
                                            <td style="width: 10%;">{{ $data->department->department_name }}</td>
                                            <td style="width: 10%;">{{ $data->email }}</td>
                                            <td style="width: 10%;">{{ $data->batch }}</td>
                                            <td style="width: 10%;">{{ $data->section }}</td>
                                            <td>
                                                <span class="edit">
                                                    <a href="{{ url('/student/'.$data->id.'/edit') }}"
                                                       class="linka fancybox fancybox.ajax labelSize2 btn btn-primary"
                                                       data-popup="tooltip" title data-original-title="Edit_Data"><i
                                                                class="fa fa-pencil"></i></a>
                                                </span>
                                                <span class="delete">
                                                    <form method="delete" id="deleteForm">
                                                    {{ csrf_field() }}
                                                    <a class="btn btn-danger"
                                                       onclick="deleteData(<?php echo $data->id ?>)"
                                                       data-popup="tooltip" title data-original-title="Delete_Data"><i
                                                                class="fa fa-trash"></i></a>
                                                </form>
                                                </span>


                                            </td>
                                        </tr>

                                    @endforeach @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection