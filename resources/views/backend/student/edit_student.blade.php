
<div class="col-sm-12" style="width: 800px;">

    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li class="active btn btn-primary btn-xs" style="background: #1D2841;">
                    <a href="#" style="color: #ddd;">
                        <i class="icon-plus3"></i> Update Subject
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content" >
        <!-- Main content -->
        <div class="row">
            <div class="col-lg-12">
                <!-- main page sources -->
                <div class="panel panel-flat">
                    <!-- Form validation -->

                    <div class="panel-body">
                        <form id="updateForm" class="form-horizontal form-validate-jquery" method="patch">

                            <fieldset class="content-group">
                                <!-- Write Your Code Here -->
                                {{ csrf_field() }}
                                <input type="hidden" name="id" id="idd" value="{{ $editData->id }}">

                                <!-- Form Input Field -->
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Student Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="name" value="{{$editData->name}}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Department</label>
                                        <div class="col-sm-8">
                                            <select name="department_id" class="form-control" required>
                                                <option>Select</option>
                                                @foreach($getDepartment as $data)
                                                    <option value="{{$data->id}}"<?php if ($data->id == ($editData->department_id)) echo ' selected="selected"'; ?>>{{$data->department_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Father's Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="father_name" value="{{$editData->father_name}}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Mother's Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="mother_name" value="{{$editData->mother_name}}" required>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Email</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="email" value="{{$editData->email}}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Password</label>

                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="password" value="{{$editData->password}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Contact Number</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="contact_number" value="{{$editData->contact_number}}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Address</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="address" value="{{$editData->address}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Batch</label>

                                        <div class="col-sm-8">
                                            <select name="batch" class="form-control" required>
                                                <option>Select</option>
                                                <option value="Regular" <?php if ($editData->batch== 'Regular') echo ' selected="selected"'; ?>>Regular</option>
                                                <option value="Pre-requisite" <?php if ($editData->batch== 'Pre-requisite') echo ' selected="selected"'; ?>>Pre-requisite</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Section</label>

                                        <div class="col-sm-8">
                                            <select name="section" class="form-control" required>
                                                <option>Select</option>
                                                <option value="a" <?php if ($editData->section== 'a') echo ' selected="selected"'; ?>>a</option>
                                                <option value="b" <?php if ($editData->section== 'b') echo ' selected="selected"'; ?>>b</option>
                                                <option value="c" <?php if ($editData->section== 'c') echo ' selected="selected"'; ?>>c</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Image</label>

                                        <div class="col-sm-6">
                                            <input type="file" name="image" required class="form-control image">
                                            <input type="hidden" name="fld_image" value="{{$editData->image}}" class="form-control image">
                                        </div>
                                        <div class="col-sm-2">
                                            @if($editData->image)

                                                <img src="{{ asset('images/student_images/'.$editData->image) }}"
                                                     height="50" width="50" class="image-show">
                                            @else
                                                <img src="{{ asset('backend_assets/no_image.png') }}" height="50"
                                                     width="50" class="image-show">

                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label">Signature</label>

                                        <div class="col-sm-6">
                                            <input type="file" name="signature" required class="form-control image">
                                            <input type="hidden" name="fld_signature" value="{{$editData->signature}}" class="form-control image">
                                        </div>
                                        <div class="col-sm-2">
                                            @if($editData->signature)

                                                <img src="{{ asset('images/student_signatures/'.$editData->signature) }}"
                                                     height="50" width="50" class="image-show">
                                            @else
                                                <img src="{{ asset('backend_assets/no_image.png') }}" height="50"
                                                     width="50" class="image-show">

                                            @endif
                                        </div>
                                    </div>
                                </div>


                            </fieldset>

                            <div class="text-right">
                                <button type="button" id="UpdateData" class="btn btn-success">Update <i class="icon-arrow-right14 position-right"></i></button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <!-- /main page sources -->
        </div>
        <!-- /main content -->
    </div>
</div>

@include('backend.ajax_request')

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.image-show').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".image").change(function(){
        readURL(this);
    });
</script>

