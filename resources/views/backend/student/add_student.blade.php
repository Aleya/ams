@extends('layouts.backend_admin.master')
@section('student', 'active')
@section('add_student', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Student</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="#" class="form-horizontal InsertForm">
                @csrf()
                <!-- Request Url -->
                    <input type="hidden" id="Url" value="/student">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Student Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" placeholder="Student Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Department</label>
                                <div class="col-sm-8">
                                    <select name="department_id" class="form-control" required>
                                        <option>Select</option>
                                        @foreach($getDepartment as $data)
                                            <option value="{{$data->id}}">{{$data->department_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Father's Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="father_name" placeholder="Father's Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Mother's Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="mother_name" placeholder="Mother's Name" required>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Password</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="contact_number" placeholder="Contact Number" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="address" placeholder="Address" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Batch</label>

                                <div class="col-sm-8">
                                    <select name="batch" class="form-control" required>
                                        <option>Select</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Pre-requisite">Pre-requisite</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Section</label>

                                <div class="col-sm-8">
                                    <select name="section" class="form-control" required>
                                        <option>Select</option>
                                        <option value="a">a</option>
                                        <option value="b">b</option>
                                        <option value="c">c</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Image</label>

                                <div class="col-sm-6">
                                    <input type="file" name="image" required id="image" onclick="imageShow()" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <img src="{{ asset('backend_assets/no_image.png') }}" height="50" width="50" id="image-show">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Signature</label>

                                <div class="col-sm-6">
                                    <input type="file" name="signature" required id="signature" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <img src="{{ asset('backend_assets/no_image.png') }}" height="50" width="50" class="signature-show">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 100px">Save</button>
                        <button type="reset" class="btn btn-default pull-right" id="reset" style="margin-right: 50px">Reset</button> &nbsp;

                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">List Semester</h3>
                </div>
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 20px">
                            <table id="example2" class="table table-bordered table-hover dataTable" >
                                <thead>
                                <tr role="row">
                                    <th >Student Name</th>
                                    <th >Department</th>
                                    <th >Email</th>
                                    <th >Contact Number</th>
                                    <th >Address</th>
                                    <th >Batch</th>
                                    <th >Section</th>
                                    <th >Image</th>
                                    <th >Signature</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                @if ($getData)
                                    @foreach ($getData as $data)

                                        <tr>
                                            <td style="width: 10%;">{{ $data->name }}</td>
                                            <td style="width: 10%;">{{ $data->department->department_name }}</td>
                                            <td style="width: 10%;">{{ $data->email }}</td>
                                            <td style="width: 10%;">{{ $data->contact_number }}</td>
                                            <td style="width: 10%;">{{ $data->address }}</td>
                                            <td style="width: 10%;">{{ $data->batch }}</td>
                                            <td style="width: 10%;">{{ $data->section }}</td>
                                            <td style="width: 15%;">
                                                <img src="{{ asset('images/student_images/'.$data->image) }}" style="height: 50px; width: 50px;">
                                            </td>
                                            <td style="width: 15%;">
                                                <img src="{{ asset('images/student_signatures/'.$data->signature) }}" style="height: 50px; width: 50px;">
                                            </td>

                                            <td>
                                                <span class="edit">
                                                    <a href="{{ url('/student/'.$data->id.'/edit') }}"
                                                       class="linka fancybox fancybox.ajax labelSize2 btn btn-primary"
                                                       data-popup="tooltip" title data-original-title="Edit_Data"><i
                                                                class="fa fa-pencil"></i></a>
                                                </span>
                                                <span class="delete">
                                                    <form method="delete" id="deleteForm">
                                                    {{ csrf_field() }}
                                                    <a class="btn btn-danger"
                                                       onclick="deleteData(<?php echo $data->id ?>)"
                                                       data-popup="tooltip" title data-original-title="Delete_Data"><i
                                                                class="fa fa-trash"></i></a>
                                                </form>
                                                </span>


                                            </td>
                                        </tr>

                                    @endforeach @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

