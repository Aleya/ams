@extends('layouts.backend_admin.master')
@section('multi_purpose', 'active')
@section('add_multi_purpose', 'active')
@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Multi Purpose</h3>
        </div>
        <div class="box-body">
            Start creating your amazing application!
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
@endsection