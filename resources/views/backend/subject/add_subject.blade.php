@extends('layouts.backend_admin.master')
@section('subject', 'active')
@section('add_subject', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Subject</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="#" class="form-horizontal InsertForm">
                    @csrf()
                    <!-- Request Url -->
                        <input type="hidden" id="Url" value="/subject">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Department</label>

                            <div class="col-sm-8">
                                <select name="department_id" class="form-control">
                                    <option>Select</option>
                                    @foreach($getDepartment as $data)
                                        <option value="{{$data->id}}">{{$data->department_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Semester</label>
                            <div class="col-sm-8">
                                <select name="semester_id" class="form-control">
                                    <option>Select</option>
                                    @foreach($getSemester as $data)
                                        <option value="{{$data->id}}">{{$data->semester_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Subject Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="subject_name" placeholder="Subject Name" required>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 100px">Save</button>
                        <button type="reset" class="btn btn-default pull-right" id="reset" style="margin-right: 50px">Reset</button> &nbsp;

                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">List Subject</h3>
                </div>
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 20px">
                            <table id="example2" class="table table-bordered table-hover dataTable" >
                                <thead>
                                <tr role="row">
                                    <th >Subject Name</th>
                                    <th >Department</th>
                                    <th >Semester</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php $s=1; ?>
                                @if ($getData)
                                    @foreach ($getData as $data)

                                        <tr>
                                            <td style="width: 20%;">{{ $data->subject_name }}</td>
                                            <td style="width: 20%;">{{$data->department->department_name}}</td>
                                            <td style="width: 20%;">{{$data->semester->semester_name}}</td>

                                            <td>
                                                <span class="edit">
                                                    <a href="{{ url('/subject/'.$data->id.'/edit') }}"
                                                       class="linka fancybox fancybox.ajax labelSize2 btn btn-primary"
                                                       data-popup="tooltip" title data-original-title="Edit_Data"><i
                                                                class="fa fa-pencil"></i></a>
                                                </span>
                                                <span class="delete">
                                                    <form method="delete" id="deleteForm">
                                                    {{ csrf_field() }}
                                                    <a class="btn btn-danger"
                                                       onclick="deleteData(<?php echo $data->id ?>)"
                                                       data-popup="tooltip" title data-original-title="Delete_Data"><i
                                                                class="fa fa-trash"></i></a>
                                                </form>
                                                </span>


                                            </td>
                                        </tr>

                                    @endforeach @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection