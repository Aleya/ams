@extends('layouts.backend_admin.master')
@section('teacher', 'active')
@section('add_teacher', 'active')
@section('content')
    <div class="box">
        <div class="box-body">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Teacher</h3>
                </div>
                <!-- form start -->
                <form id="Form" method="POST"  action="#" class="form-horizontal InsertForm">
                @csrf()
                <!-- Request Url -->
                    <input type="hidden" id="Url" value="/teacher">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Teacher Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" placeholder="Student Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Department</label>

                                <div class="col-sm-8">
                                    <select name="department_id" onchange="getSubject(this.value)" class="form-control" required>
                                        <option value="">Select</option>
                                        @foreach($getDepartment as $data)
                                            <option value="{{$data->id}}">{{$data->department_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Subject Name</label>

                                <div class="col-sm-8">
                                    <select name="subject_id" id="subjects" class="form-control" required>
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="contact_number" placeholder="Contact Number" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Password</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="address" placeholder="Address" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Semester</label>

                                <div class="col-sm-8">
                                    <select name="semester_id" class="form-control">
                                        <option>Select</option>
                                        @foreach($getSemester as $data)
                                            <option value="{{$data->id}}">{{$data->semester_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label">Image</label>

                                <div class="col-sm-6">
                                    <input type="file" name="image" required id="image" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <img src="{{ asset('backend_assets/no_image.png') }}" height="50" width="50" id="image-show">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 100px">Save</button>
                        <button type="reset" class="btn btn-default pull-right" id="reset" style="margin-right: 50px">Reset</button> &nbsp;

                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">List Semester</h3>
                </div>
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12" style="padding-top: 20px">
                            <table id="example2" class="table table-bordered table-hover dataTable" >
                                <thead>
                                <tr role="row">
                                    <th >Teacher Name</th>
                                    <th >Department</th>
                                    <th >Email</th>
                                    <th >Contact Number</th>
                                    <th >Address</th>
                                    <th >Batch</th>
                                    <th >Section</th>
                                    <th >Image</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                @if ($getData)
                                    @foreach ($getData as $data)

                                        <tr>
                                            <td style="width: 10%;">{{ $data->name }}</td>
                                            <td style="width: 10%;">{{ $data->department->department_name }}</td>
                                            <td style="width: 10%;">{{ $data->subject->subject_name }}</td>
                                            <td style="width: 10%;">{{ $data->semester->semester_name }}</td>
                                            <td style="width: 10%;">{{ $data->email }}</td>
                                            <td style="width: 10%;">{{ $data->contact_number }}</td>
                                            <td style="width: 10%;">{{ $data->address }}</td>
                                            <td style="width: 15%;">
                                                <img src="{{ asset('images/teacher_images/'.$data->image) }}" style="height: 50px; width: 50px;">
                                            </td>

                                            <td>
                                                <span class="edit">
                                                    <a href="{{ url('/teacher/'.$data->id.'/edit') }}"
                                                       class="linka fancybox fancybox.ajax labelSize2 btn btn-primary"
                                                       data-popup="tooltip" title data-original-title="Edit_Data"><i
                                                                class="fa fa-pencil"></i></a>
                                                </span>
                                                <span class="delete">
                                                    <form method="delete" id="deleteForm">
                                                    {{ csrf_field() }}
                                                    <a class="btn btn-danger"
                                                       onclick="deleteData(<?php echo $data->id ?>)"
                                                       data-popup="tooltip" title data-original-title="Delete_Data"><i
                                                                class="fa fa-trash"></i></a>
                                                </form>
                                                </span>


                                            </td>
                                        </tr>

                                    @endforeach @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
        function getSubject(depID) {
            if (depID == "") {
                return false;
            }
            $.ajax({
                type: "get",
                url: "/getSubjects/" + depID,
                dataType: "JSON",
                success: function (subjects) {
                    $('#subjects').html("");

                    $('#subjects').append($('<option>', {
                        value: "",
                        text: "Select any"
                    }));

                    $.each(subjects, function (i, item) {
                        $('#subjects').append($('<option>', {
                            value: item.id,
                            text: item.subject_name
                        }));
                    });
                }
            });

        }
    </script>
@endsection