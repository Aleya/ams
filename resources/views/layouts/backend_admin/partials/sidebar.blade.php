<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('backend_assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                {{--          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@yield('manage_roles')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Manage Roles</span>
                </a>
            </li>
            <li class="@yield('manage_permission')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Manage Permission</span>
                </a>
            </li>
            <li class="treeview @yield('manage_user')">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Manage User</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_user')"><a href="{{ url('/User/create') }}"><i class="fa fa-circle-o"></i> Create User</a></li>
                    <li class="nav-item @yield('user_list')"><a href="{{ url('/User') }}"><i class="fa fa-circle-o"></i> User List</a></li>
                </ul>
            </li>
            <li class="treeview @yield('multi_purpose')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Multi Purpose</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_multi_purpose')"><a href="{{ url('/MultiPurposePage/create') }}"><i class="fa fa-circle-o"></i> Multi Purpose Page</a></li>
                    <li><a href="{{ url('/MultiPurposePage/create') }}"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                </ul>
            </li>

            <li class="treeview @yield('department')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Department</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_department')"><a href="{{ url('/department/create') }}"><i class="fa fa-circle-o"></i>Add Department</a></li>
                 </ul>
            </li>

            <li class="treeview @yield('semester')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Semester</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_semester')"><a href="{{ url('/semester/create') }}"><i class="fa fa-circle-o"></i>Add Semester</a></li>
                  </ul>
            </li>

            <li class="treeview @yield('student')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Student</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_student')"><a href="{{ url('/student/create') }}"><i class="fa fa-circle-o"></i>Add Student</a></li>
{{--                    <li class="nav-item @yield('list_student')"><a href="{{ url('/Student') }}"><i class="fa fa-circle-o"></i> List Student</a></li>--}}
                </ul>
            </li>
            <li class="treeview @yield('teacher')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Teacher</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_teacher')"><a href="{{ url('/teacher/create') }}"><i class="fa fa-circle-o"></i>Add Teacher</a></li>
{{--                    <li class="nav-item @yield('list_teacher')"><a href="{{ url('/Teacher') }}"><i class="fa fa-circle-o"></i> List Teacher</a></li>--}}
                </ul>
            </li>
            <li class="treeview @yield('subject')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Subject</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_subject')"><a href="{{ url('/subject/create') }}"><i class="fa fa-circle-o"></i>Add Subject</a></li>
{{--                    <li class="nav-item @yield('list_subject')"><a href="{{ url('/subject') }}"><i class="fa fa-circle-o"></i> List Subject</a></li>--}}
                </ul>
            </li>

            <li class="treeview @yield('attendance')">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Attendance</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="nav-item @yield('add_attendance')"><a href="{{ url('/attendance/create') }}"><i class="fa fa-circle-o"></i>Add Attendance</a></li>
{{--                    <li class="nav-item @yield('list_attendance')"><a href="{{ url('/Attendance') }}"><i class="fa fa-circle-o"></i> List Attendance</a></li>--}}
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>