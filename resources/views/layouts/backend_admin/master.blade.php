<!DOCTYPE html>
<html>
@include('layouts.backend_admin.partials.head')

<style>
  .edit{
    float: left;
    margin-right: 10px;
  }
  .delete{
    float: left;
  }
</style>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <!-- =============================================== -->
@include('layouts.backend_admin.partials.header')
<!-- Left side column. contains the sidebar -->
@include('layouts.backend_admin.partials.sidebar')

<!-- =============================================== -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="content">
      <!-- Main content -->
    @yield('content')
    <!-- /.content -->
    </div>
  </div>

  <!-- /.content-wrapper -->

  @include('layouts.backend_admin.partials.footer')



</div>
<!-- ./wrapper -->
@include('layouts.backend_admin.partials.js_links')
@include('backend.ajax_request')
@include('backend.msg')
</body>
</html>


