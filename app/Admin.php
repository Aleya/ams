<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $guard  = 'admin';


    protected $fillable = [
        'name', 'email', 'email_verified_at',  'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeGet_admin_list()
    {
        return Admin::where('status','a')->orderBy('id', 'desc')->get();
    }
}
