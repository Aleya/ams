<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    public function scopeGetStudent()
    {
        return Student::select('id','name','father_name','mother_name','email'
            ,'contact_number','address','image','signature','department_id','batch','section')
            ->where('status','a')->orderBy('id','desc')->get();
    }

    public function department()
    {
        return $this->hasOne('App\Model\Department', 'id', 'department_id');
    }
}
