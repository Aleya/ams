<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    public function scopeGetSubject()
    {
        return Subject::select('id','subject_name','department_id','semester_id')
            ->where('status','a')->orderBy('id','desc')->get();
    }


    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function semester()
    {
        return $this->hasOne(Semester::class, 'id', 'semester_id');
    }
}
