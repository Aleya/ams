<?php

namespace App\Model;

use \App\Model\Subject;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];

    public function scopeGetDepartment()
    {
        return Department::select('id','department_name')
            ->where('status','a')->orderBy('id','desc')->get();
    }

    public function getSubject(){
        return $this->hasMany(Subject::class, 'department_id', 'id');
    }
}
