<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $guarded = [];

    public function scopeGetSemester()
    {
//        return Semester::select('id','semester_name')
//            ->where('status','a')->orderBy('id','desc')->get();

        return Semester::select('id','semester_name')
            ->where('status','a')->get();
    }
}
