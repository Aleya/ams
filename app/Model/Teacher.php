<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $guarded = [];

    public function scopeGetTeacher()
    {
        return Teacher::select('id','subject_id','department_id','semester_id','name',
            'email','image','contact_number','address')
            ->where('status','a')->orderBy('id','desc')->get();
    }
    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }
    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }
    public function semester()
    {
        return $this->hasOne(Semester::class, 'id', 'semester_id');
    }
}
