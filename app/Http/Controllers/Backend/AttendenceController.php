<?php

namespace App\Http\Controllers\Backend;

use App\Attendence;
use App\Model\Department;
use App\Model\Semester;
use App\Model\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $getDepartment  = Department::GetDepartment();
        $getSemester    = Semester::GetSemester();
        return view('backend.portal.user_dashboard.teacher.attendence.find_attendence_list',compact('getDepartment','getSemester'));
    }


    public function studentList(Request $request)
    {

        $getStudent  = Student::where('department_id',$request->department_id)
                         ->where('batch',$request->batch)
                         ->where('section',$request->section)
                         ->where('status','a')
                         ->get();
//        dd($request->all());
        return view('backend.portal.user_dashboard.teacher.attendence.student_attendence_list',compact('getStudent','getSemester'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.attendance.add_attendance');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function show(Attendence $attendence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendence $attendence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendence $attendence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendence $attendence)
    {
        //
    }

    public function getSubjects(Department $department){
        echo json_encode($department->getSubject);
    }
}
