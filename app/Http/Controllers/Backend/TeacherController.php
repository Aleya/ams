<?php

namespace App\Http\Controllers\Backend;

use App\Model\Department;
use App\Model\Semester;
use App\Model\Subject;
use App\Model\Teacher;
use App\Traits\Utility;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
//Trait
use App\Traits\ImageUpload;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{
    use Utility;
    use ImageUpload;

    public function index()
    {
        return redirect('teacher/create');
    }


    public function create()
    {
        $title          = "Teacher Entry";
        $getData        = Teacher::GetTeacher();
        $getDepartment  = Department::GetDepartment();
        $getSemester    = Semester::GetSemester();

        return view('backend.teacher.add_teacher',
            compact('title', 'getData','getDepartment','getSemester'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('type',"teacher");
        $request->offsetSet('password', Hash::make($request->password));
        if($this->validationCheck($request)):
            $user       = \App\User::create($request->all());
            $request->offsetSet('users_id',$user->id);

            $res        = Teacher::create($request->except(['password','type']));

            $image      = Input::file('image');
            $fpath      = 'images/teacher_images/';
            $fileName1  = $this->UploadFile($image, $fpath);

            $img = Teacher::find($res->id);
            $img->update([ 'image'     => $fileName1 ]);

            $this->after_process_message($res, "Save");
        endif;
    }

    public function show(Teacher $teacher)
    {
        return $teacher;
    }

    public function edit(Teacher $teacher)
    {
        $editData = $teacher;
        $getSubject    = Subject::GetSubject();
        return view('backend.teacher.edit_teacher', compact('editData','getSubject'));
    }

    public function update(Request $request, Teacher $teacher)
    {
        $res        = $teacher->update($request->all());

        $image      = Input::file('image');
        $oldImage   = $request->fld_image;
        $fpath      = 'images/teacher_images/';

        if(!empty($image)){
            $fileName   = $this->UploadFile($image, $fpath);
            $teacher->update([ 'image' => $fileName ]);
            if(!empty($oldImage)): unlink($fpath.$oldImage); endif;
        }

        $this->after_process_message($res, "Updated");
    }

    public function destroy(Teacher $teacher)
    {
        $res        = $teacher->update(['status' => 'd']);
        $user       =  User::where('id',$teacher->users_id)->update(['status' => 'd']);
        $this->after_process_message($res, "Delete");
    }


    // Validation Rules=======
    public function rules()
    {
        return [
            'name'            => 'required',
            'department_id'   => 'required',
            'subject_id'      => 'required',
            'email'           => 'required',
            'password'        => 'required',
            'address'         => 'required',
            'semester_id'     => 'required',
            'contact_number'  => 'required',
            'image'           => 'required'
        ];
    }

    // Validation Message=======
    public function messages()
    {
        return [
            'teacher_name.required'      => 'Teacher Name is required',
            'department_id.required'     => 'Department is required',
            'subject_id.required'        => 'Subject is required',
            'email.required'             => 'Email is required',
            'password.required'          => 'Password is required',
            'address.required'           => 'Address is required',
            'semester_id.required'       => 'Semester is required',
            'contact_number.required'    => 'Contact Number is required',
            'image.required'             => 'Image is required'
        ];
    }

}
