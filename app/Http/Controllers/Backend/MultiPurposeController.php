<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\MultiPurpose;
use Illuminate\Http\Request;

class MultiPurposeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct() {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.multi_purpose_page.multi_purpose_page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MultiPurpose  $multiPurpose
     * @return \Illuminate\Http\Response
     */
    public function show(MultiPurpose $multiPurpose)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MultiPurpose  $multiPurpose
     * @return \Illuminate\Http\Response
     */
    public function edit(MultiPurpose $multiPurpose)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MultiPurpose  $multiPurpose
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MultiPurpose $multiPurpose)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MultiPurpose  $multiPurpose
     * @return \Illuminate\Http\Response
     */
    public function destroy(MultiPurpose $multiPurpose)
    {
        //
    }
}
