<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use App\Model\Department;
use App\Model\Semester;
use App\Model\Subject;
use Illuminate\Http\Request;
use App\Traits\Utility;

class SubjectController extends Controller
{
    use Utility;

    public function index()
    {
        return redirect('subject/create');
    }


    public function create()
    {
        $title          = "Subject Entry";
        $getData        = Subject::GetSubject();
        $getSemester    = Semester::GetSemester();
        $getDepartment  = Department::GetDepartment();

        return view('backend.subject.add_subject',
            compact('title', 'getData','getSemester','getDepartment'));
    }

    public function store(Request $request)
    {

        if($this->validationCheck($request)):
            $res = Subject::create($request->all());
            $this->after_process_message($res, "Save");
        endif;
    }

    public function show(Subject $subject)
    {
        return $subject;
    }

    public function edit(Subject $subject)
    {
        $editData = $subject;
        $getSemester    = Semester::GetSemester();
        $getDepartment  = Department::GetDepartment();
        return view('backend.subject.edit_subject', compact('editData','getSemester','getDepartment'));
    }

    public function update(Request $request, Subject $subject)
    {
        $res = $subject->update($request->all());
        $this->after_process_message($res, "Updated");
    }

    public function destroy(Subject $subject)
    {
        $res = $subject->update(['status' => 'd']);
        $this->after_process_message($res, "Delete");
    }

    // Validation Rules=======
    public function rules()
    {
        return [
            'subject_name' => 'required'
        ];
    }

    // Validation Message=======
    public function messages()
    {
        return [
            'subject_name.required' => 'Subject Name is required'
        ];
    }
}
