<?php

namespace App\Http\Controllers\Backend;

use App\Model\Department;
use App\Traits\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    use Utility;

    public function index()
    {
        return redirect('department/create');
    }


    public function create()
    {
        $title          = "Department Entry";
        $getData        = Department::GetDepartment();

        return view('backend.department.add_department',
            compact('title', 'getData'));
    }

    public function store(Request $request)
    {

        if($this->validationCheck($request)):
            $res = Department::create($request->all());
            $this->after_process_message($res, "Save");
        endif;
    }

    public function show(Department $department)
    {
        return $department;
    }

    public function edit(Department $department)
    {
        $editData = $department;
        return view('backend.department.edit_department', compact('editData'));
    }

    public function update(Request $request, Department $department)
    {
        $res = $department->update($request->all());
        $this->after_process_message($res, "Updated");
    }

    public function destroy(Department $department)
    {
        $res = $department->update(['status' => 'd']);
        $this->after_process_message($res, "Delete");
    }


    /*
     * get subject========
     */
    public function getSubjects(Department $department){
        echo json_encode($department->getSubject);
    }



    // Validation Rules=======
    public function rules()
    {
        return [
            'department_name' => 'required'
        ];
    }

    // Validation Message=======
    public function messages()
    {
        return [
            'department_name.required' => 'Semester Name is required'
        ];
    }
}
