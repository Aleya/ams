<?php

namespace App\Http\Controllers\Backend;

use App\Model\Semester;
use App\Traits\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SemesterController extends Controller
{
    use Utility;

    public function index()
    {
        return redirect('semester/create');
    }


    public function create()
    {
        $title          = "Semester Entry";
        $getData        = Semester::GetSemester();

        return view('backend.semester.add_semester',
            compact('title', 'getData'));
    }

    public function store(Request $request)
    {

        if($this->validationCheck($request)):
            $res = Semester::create($request->all());
            $this->after_process_message($res, "Save");
        endif;
    }

    public function show(Semester $semester)
    {
        return $semester;
    }

    public function edit(Semester $semester)
    {
        $editData = $semester;
        return view('backend.semester.edit_semester', compact('editData'));
    }

    public function update(Request $request, Semester $semester)
    {
        $res = $semester->update($request->all());
        $this->after_process_message($res, "Updated");
    }

    public function destroy(Semester $semester)
    {
        $res = $semester->update(['status' => 'd']);
        $this->after_process_message($res, "Delete");
    }


    // Validation Rules=======
    public function rules()
    {
        return [
            'semester_name' => 'required'
        ];
    }

    // Validation Message=======
    public function messages()
    {
        return [
            'semester_name.required' => 'Semester Name is required'
        ];
    }
}
