<?php

namespace App\Http\Controllers\Backend;

use App\Model\Department;
use App\Model\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

//Trait
use App\Traits\ImageUpload;
use App\Traits\Utility;

class StudentController extends Controller
{
    use Utility;
    use ImageUpload;

    public function index()
    {
        return redirect('student/create');
    }


    public function create()
    {
        $title          = "Student Entry";
        $getData        = Student::GetStudent();
        $getDepartment  = Department::GetDepartment();
        return view('backend.student.add_student',
            compact('title', 'getData','getDepartment'));
    }

    public function store(Request $request)
    {

        $request->offsetSet('type',"student");
        $request->offsetSet('password', Hash::make($request->password));
        if($this->validationCheck($request)):

            $user       = \App\User::create($request->all());
            $request->offsetSet('users_id',$user->id);

            $res        = Student::create($request->except(['password','type']));

            $image      = Input::file('image');
            $fpath      = 'images/student_images/';
            $fileName1  = $this->UploadFile($image, $fpath);

            $signatures = Input::file('signature');
            $fpath      = 'images/student_signatures/';
            $fileName2  = $this->UploadFile($signatures, $fpath);

            $img = Student::find($res->id);
            $img->update([ 'image'     => $fileName1 ]);
            $img->update([ 'signature' => $fileName2 ]);
            $this->after_process_message($res, "Save");
        endif;
    }

    public function show(Student $student)
    {
        return $student;
    }

    public function edit(Student $student)
    {
        $editData = $student;
        $getDepartment  = Student::GetStudent();
        return view('backend.student.edit_student', compact('editData','getDepartment'));
    }

    public function update(Request $request, Student $student)
    {

        $res        = $student->update($request->all());

        $image      = Input::file('image');
        $oldImage   = $request->fld_image;
        $fpath      = 'images/student_images/';

        if(!empty($image)){
            $fileName   = $this->UploadFile($image, $fpath);
            $student->update([ 'image' => $fileName ]);
            if(!empty($oldImage)): unlink($fpath.$oldImage); endif;
        }

        $sign       = Input::file('signature');
        $oldSign    = $request->fld_signature;
        $Spath      = 'images/student_signatures/';

        if(!empty($image)){
            $fileName   = $this->UploadFile($sign, $Spath);
            $student->update([ 'signature' => $fileName ]);
            if(!empty($oldSign)): unlink($Spath.$oldSign); endif;
        }

        $this->after_process_message($res, "Updated");
    }

    public function destroy(Student $student)
    {
        $res = $student->update(['status' => 'd']);
        $this->after_process_message($res, "Delete");
    }


    // Validation Rules=======
    public function rules()
    {
        return [
            'name'             => 'required',
            'department_id'    => 'required',
            'father_name'      => 'required',
            'mother_name'      => 'required',
            'email'            => 'required',
            'password'         => 'required',
            'contact_number'   => 'required',
            'address'          => 'required',
            'batch'            => 'required',
            'section'          => 'required',
            'image'            => 'required',
            'signature'        => 'required'
        ];
    }

    // Validation Message=======
    public function messages()
    {
        return [
            'student_name.required'       =>  'Subject Name is required',
            'department_id.required'      => 'Department is required',
            'father_name.required'        => 'Father Name is required',
            'mother_name.required'        => 'Mother Name is required',
            'email.required'              => 'Email is required',
            'password.required'           => 'Password is required',
            'contact_number.required'     => 'Contact Number is required',
            'address.required'            => 'Address is required',
            'batch.required'              => 'Batch is required',
            'section.required'            => 'Section is required',
            'image.required'              => 'Image is required',
            'signature.required'          => 'Signature is required'
        ];
    }
}
