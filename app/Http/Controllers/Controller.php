<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // after process message=============
    protected function after_process_message($res=null, $type=null)
    {
        if($res):
            $data['success'] =  $type." successfully!!";
            echo json_encode($data);
        else:
            $data['error']   =  $type." unsuccessfully!!";
            echo json_encode($data);
        endif;
    }


    // Validation Check========
    public function validationCheck($request=null)
    {
        $rules      = $this->rules();
        $messages   = $this->messages();
        $v          = Validator::make( $request->all(), $rules, $messages );

        if($v->fails())
        {
            $data['validation'] = $v->errors();
            echo json_encode($data);
        }else{
            return true;
        }
    }

}
