<?php

namespace App\Http\Controllers;

use App\Model\Student;
use App\Model\Teacher;
use App\Portal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PortalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showLogin()
    {
        return view('backend.portal.login.portal');
    }

    public function userLogin(Request $request)
    {
        $request->validate([
            $request->user_type => 'required',
            $request->email => 'required|string',
            $request->password => 'required|min:6',
        ]);

        $user = $request->user_type;

        return view('backend.portal.user_dashboard.user_dashboard',compact('user'));
    }

    public function passwordChangeShow()
    {
        return view('backend.portal.user_dashboard.password_change');
    }

    //password change==============
    public function passwordChange(Request $request)
    {
        if (Auth::guard('web')->validate(['password' => $request->currentPassword, 'id' => $request->userID])) {
            $v = Validator::make($request->all(), [
                'password'      => 'required|min:6|required_with:re-password|same:re-password',
                're-password'   => 'required|min:6',
            ]);
            if ($v->fails()) {
                $data['validation'] =  $v->errors();
                echo json_encode($data);
            } else {
                $password = Hash::make($request->password);
                User::where('id', $request->userID)
                    ->update(['password' => $password]);
                $data['success'] = "Password change successfully!!";
                echo json_encode($data);
            }
        } else {
            $data['notice'] = "Current password do not match!!";
            echo json_encode($data);
        }
    }

    public function index()
    {
        //
    }

    public function studentEditProfile()
    {
        $user = Student::find(Auth::user()->student->id);
        return view('backend.portal.user_dashboard.student.edit_student_profile',compact('user'));
    }

    public function studentUpdateProfile(Request $request)
    {
        $user = Student::find(Auth::user()->student->id);

        $res        = $user->update($request->except('image','signature'));

        $image      = Input::file('image');
        $oldImage   = $request->fld_image;
        $fpath      = 'images/student_images/';

        if(!empty($image)){
            $fileName   = $this->UploadFile($image, $fpath);
            $user->update([ 'image' => $fileName ]);
            if(!empty($oldImage)): unlink($fpath.$oldImage); endif;
        }

        $sign       = Input::file('signature');
        $oldSign    = $request->fld_signature;
        $Spath      = 'images/student_signatures/';

        if(!empty($image)){
            $fileName   = $this->UploadFile($sign, $Spath);
            $user->update([ 'signature' => $fileName ]);
            if(!empty($oldSign)): unlink($Spath.$oldSign); endif;
        }


        return view('backend.portal.user_dashboard.student.edit_student_profile',compact('user'));
    }

    public function teacherEditProfile()
    {
        $user = Teacher::find(Auth::user()->teacher->id);
        return view('backend.portal.user_dashboard.teacher.edit_teacher_profile',compact('user'));
    }

    public function teacherUpdateProfile(Request $request)
    {

        $user = Teacher::find(Auth::user()->teacher->id);

        $res        = $user->update($request->except('image'));

        $image      = Input::file('image');
        $oldImage   = $request->fld_image;
        $fpath      = 'images/teacher_images/';


        if(!empty($image)){
            $fileName   = $this->UploadFile($image, $fpath);
            $user->update([ 'image' => $fileName ]);
            if(!empty($oldImage)): unlink($fpath.$oldImage); endif;
        }


        return view('backend.portal.user_dashboard.teacher.edit_teacher_profile',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function show(Portal $portal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function edit(Portal $portal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portal $portal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portal $portal)
    {
        //
    }
}
