<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->string('name');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('email');
//            $table->string('password');
            $table->string('contact_number');
            $table->text('address');
            $table->string('image');
            $table->string('signature');
            $table->string('department_id');
            $table->string('batch');
            $table->string('section');
            $table->enum('status', ['a', 'd'])->default('a');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
